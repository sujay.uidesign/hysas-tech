import './App.css';
import Home from './component/Home';
import ProductIdea from './component/ProductIdea';
import Footer from './component/Footer'; <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'></link>
function App() {
  return (
    <div className='main_background'>
      <Home />
      <ProductIdea />
      <Footer />
    </div>
  );
}

export default App;
