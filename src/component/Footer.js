import React from "react";import "../assets/Scss/responsive.scss";
import "../assets/Scss/Footer.scss";
import Logo from "../assets/Images/hysas-logo.png";
import Triangle from "../assets/Images/triangle_image.webp";
import sqared from "../assets/Images/socialicon/socialicon05.png";
import facebook from "../assets/Images/socialicon/socialicon04.png";
import linkedin from "../assets/Images/socialicon/socialicon03.png";
import youtube from "../assets/Images/socialicon/socialicon02.png";

import instagram from "../assets/Images/socialicon/socialicon01.png";
function Footer() {
  return (
    <>

      <section className="Footer">
        <div className="Footer_Blog">
          <div className="responsive_footer">

          <div className="Footer_Blog_Img">
            <img src={Logo} />
            <h4>Ready to transform your business digitally?</h4>
          </div>
          <div className="Footer_Blog_Text">
            <ul>
              <h4>Recent e-guides</h4>
              <li>App Monetization Strategies: How to Make Money From an App?</li>
              <li>Electric Vehicle Software Development - A Comprehensive Guide</li>
              <li>How to Enhance the UX of a Mobile App with Design Thinking: A Comprehensive Guide</li>
              <li>Artificial Intelligence (AI) - The Next Big Thing in Logistics</li>
              <li>Mastering Cloud-Based Mobile App Development: An Insider's Guide</li>
              <li>Cloud Gaming - The Beginning of the New Gaming Era</li>
            </ul>
            <ul>
              <h4>Services</h4>
              <li>Mobile App Development</li>
              <li>UX/UI Designing</li>
              <li>Web App Development</li>
              <li>Digital Marketing</li>
              <li>Offshore Outsourcing</li>
              <li>On Demand App Development</li>  <li>Cryptocurrency App Development</li>
              <li>Hire Developer</li>
              <li>Become a Partner</li>
              <li>Write for Us</li>
            </ul>

          </div>
          <div className="Footer_Blog_Text">
            <ul>
              <h4>Expertise</h4>
              <li>Blockchain App Development</li>
              <li>Metaverse App Development</li>
              <li>AR/VR App Development</li>
              <li>NFT Marketplace Development</li>
              <li>IoT App Development</li>
            </ul>
            <ul>
              <h4>Subscribe US</h4>
              <li>Make the right business move.</li>
              <li>UX/UI Designing</li>
              <div className="mail-btn">
                <input type="email" id="email" name="email" />
                <button>Send</button>
              </div>
              <span>Your email ID is confidential.</span>
              <li className="social-pack">
                <img className="social-icon" src={sqared} />
                <img className="social-icon" src={facebook} />
                <img className="social-icon" src={linkedin} />
                <img className="social-icon" src={youtube} />
                <img className="social-icon" src={instagram} />
              </li>

            </ul>
          </div>
          </div>

        </div>
        <div className="footer-right">
          <p>© 2023 HYSAS. All Rights Reserved</p>
          <h6>Privacy Policy</h6>
          <h6>Terms & Conditions</h6>
          <h6>Terms of Service</h6>
        </div>
      </section>
    </>
  );
}

export default Footer;
