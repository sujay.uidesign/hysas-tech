import React, { useRef, useState } from "react";
import { Row, Col, Carousel } from "antd";
import "../assets/Scss/home.scss";
import "../assets/Scss/responsive.scss";

import Logo from "../assets/Images/hysas-logo.png";
import Linkedin from "../assets/Images/linkedin.svg";
import Twitter from "../assets/Images/twitter.svg";
import Insta from "../assets/Images/instagram.svg";
import Behance from "../assets/Images/behance.svg";
import Arrow from "../assets/Images/arrow.2c91c6fc.svg";
import Insight from "../assets/Images/insightsuccess.svg";
import Manifest from "../assets/Images/manifest.svg";
import Candian from "../assets/Images/canadian_venture.svg";
import GoodFirm from "../assets/Images/goodfirms.svg";
import Epicos from "../assets/Images/epicos.svg";
import ReadNow from "../assets/Images/blog_icon.723f18b9.svg";
import greyarrow from "../assets/Images/greyarrow.png";
import arroww01 from "../assets/Images/arroww01.png";
import arroww02 from "../assets/Images/arroww02.png";
import { ArrowRightOutlined } from '@ant-design/icons';
import 'bootstrap/dist/js/bootstrap.bundle'; <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'></link>
const Home = () => {
  const [isNavCollapsed, setIsNavCollapsed] = useState(true);

  const handleNavCollapse = () => setIsNavCollapsed(!isNavCollapsed);
  const btnRef = useRef(null);
  const navBtnRef = useRef(null);


  const navHandleMouseMove = (e) => {
    const a = e.pageX - navBtnRef.current.offsetLeft;
    const b = e.pageY - navBtnRef.current.offsetTop;
    navBtnRef.current.style.setProperty('--a', `${a}px`);
    navBtnRef.current.style.setProperty('--b', `${b}px`);
  };

  const handleMouseMove = (e) => {
    const x = e.pageX - btnRef.current.offsetLeft;
    const y = e.pageY - btnRef.current.offsetTop;
    btnRef.current.style.setProperty('--x', `${x}px`);
    btnRef.current.style.setProperty('--y', `${y}px`);
  };
  return (
    <>
      <div className="main-contain">
        <header>
          <div className="container-header">
            <nav className="navbar navbar-expand-lg transfers" style={{ background: 'linear-gradient(176deg, rgb(44, 49, 100) 0%, rgb(30 0 20 / 61%) 90%)' }}>
              <div className="container-fluid">
                <a className="navbar-brand" href="#">
                  <img src={Logo} />
                </a>

                <div className="flex-fill"></div>


                <i class="fa fa-bars custom-toggler navbar-toggler" style={{ color: 'white' }} type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded={!isNavCollapsed ? true : false} aria-label="Toggle navigation" onClick={handleNavCollapse}></i>


                <div class={`${isNavCollapsed ? 'collapse' : ''} navbar-collapse justify-content-end`} id="navbarsExample09">
                  <ul className="navbar-nav header_link" style={{ color: "white" }}>
                    <li className="nav-item">
                      <a className="nav-link " aria-current="page" href="#">Home</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link " aria-current="page" href="#">About Us</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link " aria-current="page" href="#">Our Product</a>
                    </li>
                    <a href="#" className="contact_btns" ref={navBtnRef} onMouseMove={navHandleMouseMove}>
                      <span>Contact us</span></a>
                  </ul>
                </div>
              </div>
            </nav>
          </div>
        </header>



        <section className="FirstSection transfers">
          <div className="container">
            <div className="transfer worldalign">
              <div className="FirstSection_Container">
                <div>
                  <h1>
                    Connecting
                    <br />
                    Bits and Bytes
                    <br />
                    for Success
                  </h1>
                  <a href="#" className="FirstSection_btn" ref={btnRef} onMouseMove={handleMouseMove}>
                    <span >Let's talk</span>
                  </a>

                </div>
              </div>

              <div className="FirstSection_Links_Container">
                <div className="FirstSection_Links">
                  <img src={Linkedin} width={18} height={25} />
                  <img src={Twitter} width={18} height={25} />
                  <img src={Insta} width={18} height={25} />
                  <img src={Behance} width={18} height={25} />
                </div>

                <div className="arrow sector">
                  <div className="FirstSection_Arrow_Container">
                    <div className="FirstSection_Arrow">
                      <img src={Arrow} width={40} height={40} />
                    </div>
                    <div className="FirstSection_ArrowLink">
                      <h4>read our blog on</h4>
                      <a href="#">
                        <h4>Progressive Web App (PWA)</h4>
                      </a>
                    </div>
                  </div>

                  <div className="FirstSection_HIGHLIGHTS">
                    <div className="FirstSection_HIGHLIGHTS_Secound-Thirt fi">
                      <span>Software  <br /> development Team
                      </span><div className="grey-row"><ArrowRightOutlined /></div>
                      <p>
                        Our dedicated software development team is a diverse group of minds working together to transform ideas into code.
                      </p>
                    </div>
                    <div className="FirstSection_HIGHLIGHTS_Secound-Thirt nd">
                      <span>Customer-Centric:
                      </span>
                      <div className="grey-row"><ArrowRightOutlined /></div>
                      <div>
                        <p> We prioritize our clients and their needs above all else.
                          Your success is our success, and we work tirelessly to exceed your expectations.
                        </p>
                      </div>
                    </div>
                    <div className="FirstSection_HIGHLIGHTS_Secound-Thirt rd">
                      <span>Expertise:
                      </span>
                      <div className="grey-row">
                        <ArrowRightOutlined />
                      </div>
                      <div>
                        <p>
                          Our team comprises a diverse group of experts who bring years of experience and deep knowledge to every project.
                          We're passionate about what we do, and it shows in our work.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div></div>
          </div>

        </section>

        <section className="SecoundSection transfers">
          <div className="container">
            <div className="container-admin">
              <Row className="central" justify={"center"}>
                <Col className="sectors" span={20}>
                  <div className="slider-sector">
                    <Carousel autoplay dots={false}>
                      <div>
                        <h3>
                          We are your Bridge to Digital Transformation and we empower your digital world
                        </h3>
                      </div>
                      <div>
                        <h3>
                          We are your Bridge to Digital Transformation and we empower your digital world
                        </h3>
                      </div>
                      <div>
                        <h3>
                          We are your Bridge to Digital Transformation and we empower your digital world
                        </h3>
                      </div>
                      <div>
                        <h3>
                          We are your Bridge to Digital Transformation and we empower your digital world
                        </h3>
                      </div>


                    </Carousel>
                    <div className="arrow-w">
                      <img src={arroww01} />
                      <img src={arroww02} />
                    </div>
                  </div>
                  <div className="SecoundSection_Logo">
                    <img src={Insight} width={225} height={100} />
                    <img src={Manifest} width={140} height={100} />
                    <img src={Candian} width={150} height={100} />
                    <img src={GoodFirm} width={180} height={100} />
                    <img src={Epicos} width={225} height={100} />
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default Home;
