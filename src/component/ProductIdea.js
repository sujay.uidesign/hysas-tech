import React from 'react';
import Select from 'react-select';
import "../assets/Scss/responsive.scss";
import "../assets/Scss/home.scss";
import TdBank from "../assets/Images/td_bank_new.webp";
import Carbon from "../assets/Images/carbonhound_new.webp";
import Hedge from "../assets/Images/hedgelegal_new.webp";
import Traf from "../assets/Images/traf_new.webp";
import Fino from "../assets/Images/finobuddy_new.webp";
import Akkamai from "../assets/Images/akamai_new.webp";
import Gord from "../assets/Images/gardmoi_new.webp";
import Fuss from "../assets/Images/fuss_mobile_new.webp";
import McGill from "../assets/Images/mcgill_new.webp";
import Mopile from "../assets/Images/mobile.webp";
import Watch from "../assets/Images/watch.webp";
import desktop from "../assets/Images/desktop.webp";
import settings from "../assets/Images/accordion_img_1.webp";
import smallSettings from "../assets/Images/service_small_icon_1.webp";
import Arrow from "../assets/Images/arrow.2c91c6fc (1).svg";
import Staff from "../assets/Images/service_small_icon_2.webp";
import Analytics from "../assets/Images/service_small_icon_3.webp";
import Digital from "../assets/Images/service_small_icon_4.webp";
import nest from "../assets/Images/nest.jpg";
import crush from "../assets/Images/crush.jpg";
import hop from "../assets/Images/hop.jpg";
import tun from "../assets/Images/tun.jpg";
import nix from "../assets/Images/nix.jpg";
import app from "../assets/Images/app.jpg";
import arrow from "../assets/Images/arrow.png";
import warrow from "../assets/Images/warrow.png";
import boxarrow from "../assets/box-arrow.png";
import bg from "../assets/Images/bg.jpg";
import india from "../assets/Images/india.jpg";
import usa from "../assets/Images/usa.jpg";
import uea from "../assets/Images/uea.jpg";
import findia from "../assets/Images/flag/india.jpg";
import fusa from "../assets/Images/flag/usa.jpg";
import "../assets/Scss/responsive.scss";
import fmail from "../assets/Images/flag/mail.jpg";
import { ArrowRightOutlined } from '@ant-design/icons';

import { Row, Col } from "antd";
const countryOptions = [
  { flag: '../assets/Images/flag/Flag_of_India.png', value: '+91', },
  { flag: '../assets/Images/flag/Flag_of_the_United_States.png', value: '+1', },
];
const customStyles = {
  option: (provided) => ({
    ...provided,
    backgroundRepeat: 'no-repeat',
    backgroundSize: '20px 15px',
    paddingLeft: '30px',
    backgroundPosition: 'center left',
  }),
};
const ProductIdea = () => {
  return (
    <>
      <div className='container'>
        <section className='transfer'>
          <div className='ProductIdea '>

            <div className='ProductIdea_FirstSection_text'>
              <div>
                <h2>Cutting-Edge Technology:
                </h2>
                <p>From Data to Decision – We've Got You Covered</p>
              </div>
              <div className='port-folio'>
                <h2>View All Portfolio</h2>
                <img src={boxarrow} />
              </div>
            </div>


            <div className='ProductIdea_FirstSection'>
              <div className='product-sector'><img src={nest} /></div>
              <div className='product-sector'><img src={crush} /></div>
              <div className='product-sector'><img src={hop} /></div>
            </div>


          </div>
        </section>
        <section className='ProductIdea_SecoundSection transfer'>
          <Row className='product-maintain' >
            <Col >
              <div className='ProductIdea_SecoundSection_text'>
                <span>Featured Blog
                </span>
                <p>Check out our guides to stay updated with the market and economy.
                </p>
              </div>
            </Col>
            <Col className='product-sector'>
              <div>
                <div className='ProductIdea_SecoundSection_Img text1'>
                  <div><img src={app} /></div>
                  <div className='product-second-text1'>
                    <span>Blog
                    </span>
                    <p>Home Services Apps - The Future of Tomorrow|App Development Guide
                    </p>
                    <h6>Dec 23,2021
                    </h6>
                    <button className='read-btn'>Read more    <ArrowRightOutlined /></button>
                  </div>
                </div>
                <div className='ProductIdea_SecoundSection_Img text2'>
                  <div><img src={tun} /></div>

                  <div className='product-second-text2'>
                    <span>Blog</span>
                    <p>Complete Guide On Taxi Booking App Development - with Benefits And Features
                    </p>
                    <h6>Dec 8,2021</h6>
                    <button className='read-btn'>Read more    <ArrowRightOutlined /></button>

                  </div>
                </div></div>
              <div className='ProductIdea_SecoundSection_Img text3'>
                <div className='product-second-text3'>
                  <span>E Guide

                  </span>
                  <p>App Monetization Strategies:How to make <br></br>Money From an App?</p>
                  <h6>Jun ,2023
                  </h6>
                  <button className='read-btn'>Read more    <ArrowRightOutlined /></button>

                </div>
                <div className='process-product'>
                  <img src={nix} />
                </div>

              </div>

            </Col>
            <div className='read-arrow'>
              <button className='read-btn'><label>View All</label>    <ArrowRightOutlined /></button>
            </div>
          </Row>
        </section>
        <section className='transfer'>

          <div className='black-sector'>
            {/* <img src={bg} /> */}

            <div className='black-text'>
              <h2>Proven Track Record:<br>
              </br> <span>We have a successful history of delivering IT solutions that make a significant impact on our clients' businesses.
                </span></h2>
              <div className='read-arrow'>
                <button className='read-btn'> <label>Get a Quote  </label> <ArrowRightOutlined /></button>
              </div></div>
          </div>

        </section>
        <section className='transfer'>
          <div className='ProductIdea_Services'>
            <div className='Product_our_event'>
              <div className='product-text-content'>

                <div className='ProductIdea_Services-text'>
                  <span>Get in Touch with
                  </span>
                  <h3 className='service-expert'>Our<br></br> Experts
                  </h3>
                  <p className='flex_para'>
                    <p className='para'>Your satisfaction is our ultimate goal.</p>
                    <p className='para'>We listen to your requirements, communicate transparently,</p>
                    <p className='para'>and ensure your project's success.</p>
                  </p>
                </div>
                <div className='ProductIdea_Services-text'>
                  <div>
                    <h3>Boostup Your Business</h3>
                    <p>Mobile App Development</p>
                  </div>
                  <button className='read-btn'>Become a Partner <ArrowRightOutlined />
                  </button>
                </div>
                <div className='ProductIdea_Services-text'>
                  <div>
                    <h3>Contact Info:
                    </h3>
                    <div className='contact-text'>
                      <img src={fmail} />
                      <span>sales@apptunix.com</span>
                    </div>
                    {/* <div className='contact-text'>
                      <img src={fusa} />
                      <span>+ (513) 873364
                      </span>
                    </div> */}
                    <div className='contact-text'>
                      <img src={findia} />
                      <span>+917740084615</span>
                    </div>
                  </div>

                </div>

              </div>



              <div className='ProductIdea_Services-img'>
                <h2>Let's Get to know you</h2>
                <form className='our-expert-form' action="server url" method="get|post">

                  <input type='text' id="name" placeholder='Full Name' required />
                  <input type='email' id="email" placeholder='Email' required />
                  <div class="dropdown country-flag">
                    <div class="dropdown">
                      <button onclick="toggleDropdown()" class="dropbtn">
                        <img
                          src={findia}
                          alt=""
                          style={{ width: '20px', height: '15px', marginRight: '10px', verticalAlign: 'middle' }}
                        />
                        <span class="country-code">+91</span>
                      </button>
                      <div id="myDropdown" class="dropdown-content">
                        <a href="#">
                          <img src="../assets/Images/flag/Flag_of_the_United_States.png" alt="USA" class="flag-icon" /> USA
                          <span class="country-code">+1</span>
                        </a>

                      </div>
                    </div>
                    <input pattern="[0-9]{10}" maxlength="10" type="tel" id="mobile" placeholder='Mobile Number' name="mobile"  required />

                  </div>
                  {/* <div className="country-flag">
                    <Select
                      options={countryOptions} styles={customStyles} formatOptionLabel={({ label, flag }) => (
                        <div>
                          <img
                            src={findia}
                            alt=""
                            style={{ width: '20px', height: '15px', marginRight: '10px', verticalAlign: 'middle' }}
                          />
                        </div>
                      )}
                    />
                    <input type="tel" id="mobile" placeholder='Mobile Number' name="mobile" pattern="[0-9]{10}" required />

                  </div> */}

                  <textarea id="message" placeholder='About Project' name="message" rows="4" cols="50" required></textarea>

                  <div className='read-arrow'>
                    <button className='read-btn'><label>Submit</label>  <ArrowRightOutlined /></button>
                  </div>
                </form>
              </div>
            </div>
            <div className='ProductIdea_Services_Development'>
              <div className='our-touch'>
                <div className='touch-picture'>
                  <img src={india} />
                </div>
                <div className='our-touch-text'>
                  <h2>India</h2>
                  <p>C-127 Phase vill Industrial Area Mohali, India. 160071
                  </p>
                </div>
              </div>
              <div className='our-touch '>
                <div className='touch-picture'>
                  <img src={usa} />
                </div>
                <div className='our-touch-text'>
                  <h2>USA</h2>
                  <p>Suite #304, 11200 Manchaca, Austin, Texas, US, 79748
                  </p>
                </div>
              </div>
              <div className='our-touch country'>
                <div className='touch-picture '>
                  <img src={uea} />
                </div>
                <div className='our-touch-text'>
                  <h2>UAE</h2>
                  <p>#2044, Floor 20, Burjuman Business Tower, Dubai.</p>
                </div>
              </div>
            </div>
          </div>

        </section>
      </div>
    </>
  )
  function toggleDropdown() {
    var dropdown = document.getElementById("myDropdown");
    if (dropdown.style.display === "block") {
      dropdown.style.display = "none";
    } else {
      dropdown.style.display = "block";
    }
  }

  // Close the dropdown if the user clicks outside of it
  window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdown = document.getElementById("myDropdown");
      if (dropdown.style.display === "block") {
        dropdown.style.display = "none";
      }
    }
  }

}

export default ProductIdea
